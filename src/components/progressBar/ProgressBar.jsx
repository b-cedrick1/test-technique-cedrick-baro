import { useState, useEffect } from "react";
import styles from "./ProgressBar.module.css";

export const ProgressBar = ({
  title,
  progression,
  selectedBar,
  id,
  initialStatus,
}) => {
  const [color, setColor] = useState("");
  const [localProgress, setLocalProgress] = useState(0);

  useEffect(() => {
    if (selectedBar === id || initialStatus) {
      let color = "";
      if (progression <= 25) {
        color = "#7160E8";
      } else if (26 <= progression && progression <= 50) {
        color = "#60ADE8";
      } else if (51 <= progression && progression <= 75) {
        color = "#60E8B6";
      } else if (76 <= progression && progression <= 100) {
        color = "#30DB63";
      }
      setColor(color);
      setLocalProgress(progression);
    }
  }, [progression, selectedBar, id, initialStatus, localProgress]);
  return (
    <div className={styles.progressBarMainContainer}>
      <div className={styles.label}>{title}</div>
      <div className={styles.progressBarContainer}>
        <div className={styles.progression}>{localProgress}%</div>
        <div className={styles.bar}>
          <div
            className={styles.statusBar}
            style={{
              backgroundColor: color,
              height: "15px",
              width: localProgress + "%",
            }}
          >
            &nbsp;
          </div>
        </div>
      </div>
    </div>
  );
};
