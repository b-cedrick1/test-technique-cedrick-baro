import { useEffect } from "react";
import { createRef, useState } from "react";
import { createPortal } from "react-dom";
import styles from "./Modal.module.css";

export const Modal = ({ addItem, listInValidId, close }) => {
  const inputIdRef = createRef();
  const inputTitleRef = createRef();
  const inputProgressionRef = createRef();
  const [errorMsg, setErrorMsg] = useState("");

  const portalContainer = document.createElement("div");
  document.body.appendChild(portalContainer);

  const onClose = () => {
    close();
    document.body.removeChild(portalContainer);
  };

  const validate = () => {
    const id = parseInt(inputIdRef.current.value);
    const isInvalidId = listInValidId.includes(id);
    if (isInvalidId) {
      setErrorMsg("Id déjà utilisé.");
    } else {
      const title = inputTitleRef.current.value;
      const progression = parseInt(inputProgressionRef.current.value);
      addItem(id, title, progression);
    }
  };

  useEffect(() => {
    if (errorMsg) {
      const timeoutId = setTimeout(() => {
        setErrorMsg("");
      }, 1000);
      return () => clearTimeout(timeoutId);
    }
  }, [errorMsg]);

  return createPortal(
    <div className={styles.modalContainer}>
      <div className={styles.modal}>
        <label className={styles.label}>Id de la bare de progression</label>
        <input
          ref={inputIdRef}
          className={styles.input}
          type="number"
          min={1}
        />
        <span className={styles.errorMsg}>{errorMsg}</span>
        <label className={styles.label}>Titre de la barre</label>
        <input ref={inputTitleRef} className={styles.input} type="text" />
        <label className={styles.label}>Valeur de la progression en %</label>
        <input
          ref={inputProgressionRef}
          className={styles.input}
          type="number"
          min={1}
        />
        <div>
          <button className={styles.btnCancel} onClick={onClose}>
            Annuler
          </button>
          <button className={styles.btnValidate} onClick={validate}>
            Valider
          </button>
        </div>
      </div>
    </div>,
    portalContainer
  );
};
