import { createRef, useEffect, useState } from "react";
import { ProgressBar } from "./components/progressBar/ProgressBar";
import styles from "./App.module.css";
import { Modal } from "./components/modal/Modal";
function App() {
  const [progressBar, setProgressBar] = useState([
    {
      id: 1,
      title: "Initialisation du test technique",
      progression: 50,
    },
    {
      id: 2,
      title: "Avancement de la phase de développement",
      progression: 25,
    },
  ]);

  const [selectedBar, setSelectedBar] = useState(1);
  const [initialStatus, setInitialStatus] = useState(true);
  const [openModal, setOpenModal] = useState(false);
  const [listInValidId, setListInvalidId] = useState([]);

  const selectRef = createRef();

  const updateProgession = (value) => {
    setInitialStatus(false);
    let newState = [];
    if (value === 0) {
      newState = progressBar.map((state) => {
        return state.id === selectedBar
          ? { ...state, progression: value }
          : state;
      });
    } else {
      newState = progressBar.map((state) => {
        let newValue = 0;
        if (state.progression + value <= 100) {
          newValue =
            100 - (state.progression + value) < 5
              ? 100
              : state.progression + value;
        } else {
          newValue = state.progression;
        }
        return state.id === selectedBar
          ? {
              ...state,
              progression: newValue,
            }
          : state;
      });
    }
    setProgressBar(newState);
  };

  const handleAddFivePercent = (e) => {
    updateProgession(5);
  };

  const handleAddTenPercent = () => {
    updateProgession(10);
  };

  const handleReset = () => {
    updateProgession(0);
  };

  const selectBar = () => {
    setSelectedBar(parseInt(selectRef.current.value));
  };

  const removeProgressBar = () => {
    const newState =
      progressBar.length > 1
        ? progressBar.filter((item) => item.id !== selectedBar)
        : progressBar;
    setProgressBar(newState);
  };

  const addProgressBar = () => {
    const listId = progressBar.map((item) => item.id);
    setListInvalidId(listId);
    setOpenModal(true);
  };

  const closeModal = () => {
    setOpenModal(false);
  };

  const handleAddProgressBar = (id, title, progression) => {
    const newState = [...progressBar, { id, title, progression }];
    setProgressBar(newState);
    console.log(newState);
    closeModal();
  };

  useEffect(() => {}, [progressBar.length]);
  return (
    <>
      <div className={styles.title}>
        Test technique WEB-ATRIO réalisé par BARO Cédrick <br /> réalisé le
        18/11/2022
      </div>
      {openModal && (
        <Modal
          listInValidId={listInValidId}
          close={closeModal}
          addItem={handleAddProgressBar}
        />
      )}
      {progressBar &&
        progressBar.map((item) => {
          return (
            <ProgressBar
              key={item.id}
              id={item.id}
              initialStatus={initialStatus}
              title={item.title}
              progression={item.progression}
              selectedBar={selectedBar}
            />
          );
        })}
      <div className={styles.btnContainer}>
        <div className={styles.btnGroup1}>
          <select
            ref={selectRef}
            className={styles.select}
            onChange={selectBar}
          >
            {progressBar &&
              progressBar.map((item, index) => {
                return (
                  <option key={item.id + index} value={item.id}>
                    Barre {item.id}
                  </option>
                );
              })}
          </select>
          <div className={styles.btn} onClick={handleReset}>
            Remettre à zéro le compteur
          </div>
        </div>
        <div className={styles.btnGroup2}>
          <div className={styles.btn} onClick={handleAddFivePercent}>
            Ajouter 5%
          </div>
          <div className={styles.btn} onClick={handleAddTenPercent}>
            Ajouter 10%
          </div>
          <div className={styles.btnRemoveComp} onClick={removeProgressBar}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="1em"
              height="1em"
              preserveAspectRatio="xMidYMid meet"
              viewBox="0 0 24 24"
            >
              <path
                fill="white"
                d="M18 12.998H6a1 1 0 0 1 0-2h12a1 1 0 0 1 0 2z"
              />
            </svg>
          </div>
          <div className={styles.btnAddComp} onClick={addProgressBar}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="1em"
              height="1em"
              preserveAspectRatio="xMidYMid meet"
              viewBox="0 0 24 24"
            >
              <path fill="white" d="M19 12.998h-6v6h-2v-6H5v-2h6v-6h2v6h6z" />
            </svg>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
